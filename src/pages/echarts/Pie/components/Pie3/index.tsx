import React, { useEffect, useState } from "react";
import Pie3 from "components/Echarts/Pie3";
import styles from "../../index.module.scss";

const Pie1 = () => {
  const [echartData, setEchartData] = useState<(string | number)[][]>([]);

  const getData = () => {
    const data = [];
    const num = 4;
    for (let i = 0; i < num; i++) {
      const name = `name${i + 1}`;
      const value = Math.round(Math.random() * 100);
      data.push([name, value]);
    }
    setEchartData(data);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className={styles.echart}>
      <Pie3 data={{ dataSource: echartData }} />
    </div>
  );
};

export default Pie1;
