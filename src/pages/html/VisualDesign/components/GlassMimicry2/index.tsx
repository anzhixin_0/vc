import React from "react";
import styles from "./index.module.scss";

const GlassMimicry2 = () => {
  return (
    <div className={styles.container}>
      <div className={styles.card}>
        <h1>TomatoLRX</h1>
        <h2>1111 2222 3333 4444</h2>
        <h3>Welcome it</h3>
        <h4>08 / 24</h4>
      </div>
      <div className={styles.circle}></div>
      <div className={styles.rect}></div>
    </div>
  );
};

export default GlassMimicry2;