import React from "react";
import styles from "./index.module.scss";

const AndLightButton = () => {
  return (
    <div className={styles.container}>
      <button>Hover me</button>
    </div>
  );
};

export default AndLightButton;
