import React, { useRef, useMemo } from "react";
import echarts, { getGraphic } from "@/utils/echarts.config";
import type { ChartOptions } from "@/utils/echarts.config";
import { ECHART_COMMON_COLOR } from "constants/common";
import numberFormat from "utils/numberFormat";
import BasicEchart from "../BasicEchart";

interface PieProps {
  data: {
    dataSource: (string | number)[][];
  };
  style?: Record<string, any>;
  onHoverChange?: (params: any) => void;
  onClickChange?: (params: any) => void;
}

// 获取整个图表的基础配置
const getBaseOptions = () => {
  const baseOptions: ChartOptions = {
    color: ECHART_COMMON_COLOR,
    // @ts-ignore
    graphic: getGraphic(),
    legend: {
      textStyle: {
        color: "rgba(251, 251, 251, 1)",
      },
      /*  data: [
        "Direct",
        "Marketing",
        "Search Engine",
        "Email",
        "Union Ads",
        "Video Ads",
        "Baidu",
        "Google",
        "Bing",
        "Others",
      ], */
    },
    tooltip: {
      show: true,
      trigger: "item",
      // formatter: "{a} <br/>{b}: {c} ({d}%)",
      formatter: function (params: any) {
        const { value, seriesName, percent } = params;
        // console.log("value", value, seriesName, percent);
        let str = `${seriesName} ${"<br/>"} ${value[0]} : ${numberFormat(
          value[1],
          true
        )}(${percent}%)`;
        return str;
      },
      valueFormatter: (value: any) => numberFormat(value, true) as string,
    },
    series: [
      {
        name: "Price",
        type: "pie",
        selectedMode: "single",
        radius: [0, "30%"],
        avoidLabelOverlap: false,
        label: {
          position: "inner",
          fontSize: 14,
        },
        labelLine: {
          show: false,
        },
        /* data: [
          { value: 1548, name: 'Search Engine' },
          { value: 775, name: 'Direct' },
          { value: 679, name: 'Marketing', selected: true }
        ] */
      },
      {
        name: "Access From",
        type: "pie",
        radius: ["45%", "60%"],
        labelLine: {
          length: 30,
        },
        label: {
          formatter: "{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ",
          backgroundColor: "#F6F8FC",
          borderColor: "#8C8D8E",
          borderWidth: 1,
          borderRadius: 4,
          rich: {
            a: {
              color: "#6E7079",
              lineHeight: 22,
              align: "center",
            },
            hr: {
              borderColor: "#8C8D8E",
              width: "100%",
              borderWidth: 1,
              height: 0,
            },
            b: {
              color: "#4C5058",
              fontSize: 14,
              fontWeight: "bold",
              lineHeight: 33,
            },
            per: {
              color: "#fff",
              backgroundColor: "#4C5058",
              padding: [3, 4],
              borderRadius: 4,
            },
          },
        },
        /*  data: [
          { value: 1048, name: "Baidu" },
          { value: 335, name: "Direct" },
          { value: 310, name: "Email" },
          { value: 251, name: "Google" },
          { value: 234, name: "Union Ads" },
          { value: 147, name: "Bing" },
          { value: 135, name: "Video Ads" },
          { value: 102, name: "Others" },
        ], */
      },
    ],
    dataset: {
      source: [],
    },
  };
  return baseOptions;
};

const Pie3 = ({
  data, // 数据
  style, // 样式
  onHoverChange, // 鼠标hover事件
  onClickChange, // 点击事件
}: PieProps) => {
  // 图表最终的配置数据
  const chartOptions = useMemo(() => {
    const options = getBaseOptions();
    const { dataSource } = data;
    if (!dataSource) return options;
    options.dataset = { source: dataSource };
    return options;
  }, [data]);

  return (
    <BasicEchart
      options={chartOptions}
      style={style}
      onClickChange={onClickChange}
      onHoverChange={onHoverChange}
    />
  );
};

export default Pie3;
