<h1 align="center">Visualization Collection</h1>
<div align="center">
🌀一个专注于前端视觉效果的集合应用，包含CSS动效、Canvas动画、Three.js3D、人工智能应用等案例（持续更新）
</div>

#### 在线体验(PC端)
体验地址：http://tomato-lrx.gitee.io/vc

## 应用展示(部分效果)
* 视觉设计
<img src="./src/images/readme/visualDesign.gif" alt="" width={1200}/>

* 交互设计
<img src="./src/images/readme/interactiveDesign1.gif" alt="" width={1200}/>

<img src="./src/images/readme/interactiveDesign4.gif" alt="" width={1200}/>

<img src="./src/images/readme/interactiveDesign3.gif" alt="" width={1200}/>

* Canvas动效
<img src="./src/images/readme/canvas1.gif" alt="" width={1200}/>

<img src="./src/images/readme/canvas2.gif" alt="" width={1200}/>

<img src="./src/images/readme/canvas3.gif" alt="" width={1200}/>

<img src="./src/images/readme/canvas4.gif" alt="" width={1200}/>

<img src="./src/images/readme/canvas5.gif" alt="" width={1200}/>

## 持续更新，敬请关注！
